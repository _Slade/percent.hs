module Percent.Encode where
import Data.ByteString.Lazy (ByteString)
import Data.Word (Word8)
import Data.Char (ord, chr)
import qualified Data.Map.Strict as Map
import qualified Data.ByteString.Lazy as BS

charToWord8 :: Char -> Word8
charToWord8 = fromIntegral . ord
isReserved :: Word8 -> Bool
isReserved = flip Map.member reservedChars

reservedChars :: Map.Map Word8 ()
reservedChars = Map.fromList $ zip
    (fmap (fromIntegral . ord) ['!', '*', '\'', '(', ')', ';', ':', '@', '&',
     '=', '+', '$',  ',', '/', '?', '#', '[', ']'])
     (cycle [()])

toHexDigit :: Word8 -> Word8
toHexDigit n
    |  0 <= n && n <=  9 = n + (charToWord8 '0')
    | 10 <= n && n <= 16 = n + (charToWord8 'A')
    | otherwise = error $ "Value does not fit in hex digit"
                       ++ "(perhaps you want encodeByte?)"

encode :: ByteString -> ByteString
encode _ = undefined -- TODO

encodeByte :: Word8 -> ByteString
encodeByte n
    | 0 <= n && n < 16 = BS.singleton $ toHexDigit n
    | otherwise = let (q, r) = quotRem n 16
                  in toHexDigit r `BS.cons` encodeByte q
