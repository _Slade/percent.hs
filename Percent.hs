module Percent where
import Data.ByteString.Lazy (ByteString)
import System.Environment   (getArgs)
import System.Exit          (exitSuccess)
import qualified Data.ByteString.Lazy as BS

import Percent.Decode (decode)
import Percent.Encode (encode)

main :: IO ()
main = do
    args <- getArgs
    case processArgs args of
        DecodeString str -> BS.putStrLn $ decode str
        EncodeString str -> undefined -- BS.putStrLn $ encode str
        DecodeStdin      -> BS.interact decode
        EncodeStdin      -> undefined -- BS.interact encode
        PrintHelp        -> sequence_ [putStr help, exitSuccess]

data Action =
      DecodeString String
    | DecodeStdin
    | EncodeString String
    | EncodeStdin
    | PrintHelp

processArgs :: [String] -> Action
processArgs args = case args of
    ["--decode", str] -> DecodeString str
    ["-d",       str] -> DecodeString str
    ["--decode"     ] -> DecodeStdin
    ["-d"           ] -> DecodeStdin
    [               ] -> DecodeStdin
    ["--encode", str] -> EncodeString str
    ["-e",       str] -> EncodeString str
    ["--encode"     ] -> EncodeStdin
    ["-e"           ] -> EncodeStdin
    _                 -> PrintHelp

help :: String
help = unlines
    [
        "PercentDecode",
        "OPTIONS",
        "\t--decode -d\tPerform decoding. Decodes stdin if no string is given.",
        "\t--encode -e\tPerform encoding. Decodes stdin if no string is given."
        ++ " NOT YET IMPLEMENTED" -- XXX
    ]

