module Percent.Decode where
import Data.Char (
        ord,
        chr,
        isHexDigit,
        digitToInt,
    )
import Data.ByteString.Lazy (ByteString)
import Data.Word            (Word8)
import qualified Data.List
import qualified Data.ByteString.Lazy as BS
import qualified Data.Char            as C

decode :: ByteString -> ByteString
decode str = undefined -- TODO

decodeString :: String -> String
decodeString ('%':y:z:rest) = if isHexDigit y && isHexDigit z
    then chr (digitToInt y * 16 + digitToInt z) : decodeString rest
    else error "Malformed data"
decodeString (x:y:z:rest) = x : decodeString (y:z:rest)
decodeString base = base

